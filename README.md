**LAPRES MODUL 2**

**SOAL NO 2**

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.


a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.


b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.


c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.


d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)


e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

**JAWABAN NO 2**

**2 A**

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

void listFilesRecursively(char *path);
void unzipfile() {
 pid_t child_id;
 int status;

 child_id = fork();

 if (child_id < 0) {
   exit(EXIT_FAILURE);
 }

 if (child_id == 0) {
   char *argv[] = {"mkdir", "/home/tiodwii/shift2/drakor", NULL};
   execv("/bin/mkdir", argv);
 }
 else {
    char argv[] = {"unzip", "/home/tiodwii/shift2/drakor.zip", ".png", "-d","/home/tiodwii/shift2/drakor", NULL};
    execv("/bin/unzip", argv);
 }
}

void ini_exec(char a[], char *arg[]){
   pid_t child_id;
   int status;
   child_id = fork();
   if (child_id == 0){
       execv (a, arg);
   }
   else {
       ((wait(&status))>0);
   }

}
```


**2 B**

```
void listFilesRecursively(char *basePath)
{
   char path[1000];
   struct dirent *dp;
   DIR *dir = opendir(basePath);

   if (!dir)
       return;

   while ((dp = readdir(dir)) != NULL)
   {
       if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
       {
           char file[100]= "";
           strcpy(file,dp->d_name);
           strtok(file, ";");
           //printf("%s\n", namafile);
           char namafile[100]= "/home/tiodwii/shift2/drakor/";
           strcat(namafile,file);
           char *argv[] = {"mkdir", "-p", namafile, NULL};
           ini_exec("/bin/mkdir", argv);

           strcpy(path, basePath);
           strcat(path, "/");
           strcat(path, dp->d_name);

           listFilesRecursively(path);
       }
   }

   closedir(dir);
}

int main()
{

   pid_t child_id;

   child_id = fork();

   //2a
   if(child_id != 0){
   unzipfile();

   //2b
   }else {
   char path[100] = "/home/tiodwii/shift2/drakor/";
   listFilesRecursively(path);
   }

 return 0;

}
```

**SOAL NO 3**

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

a. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

d. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

e. Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

**JAWABAN NO 3**

**3 A & B**

```
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <wait.h>

void moving (const char* path){
  pid_t child , child_id, child_id2;
  int status;
  DIR *dir;
  struct dirent *a;
  char src[9999];
  char src2[9999];
  char file[9999];
  char file2[9999];

int main() {
  pid_t child1, child2;
  int status;

  child1 = fork();
  if (child1 < 0) {
    exit(EXIT_FAILURE);
  }
  if (child1 == 0) {
    // child
    child2 = fork();
    if (child2 < 0) {
      exit(EXIT_FAILURE);
    }
  if (child2 == 0){
      // child
      char *argv1[] = {"mkdir", "-p", "/home/rahadianpra/modul2/darat/", NULL};
      execv("/bin/mkdir", argv1);
    }
    else{
      while((wait(&status))>0){
        sleep(5);
        char *argv2[] = {"mkdir", "-p", "/home/rahadianpra/modul2/air/", NULL};
        execv("/bin/mkdir", argv2);
      }
    }
  }
```

```
RAHADIAN SURYO PRAYITNO - 5025201149
CHRISTIAN J H PAKPAHAN - 5025201149
```
